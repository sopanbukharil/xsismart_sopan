﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Xsismart.Misc;
using Xsismart.Models;
using Xsismart.ViewModels;

namespace Xsismart.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly Xsismartcontext _context;
        public HomeController(ILogger<HomeController> logger, Xsismartcontext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> Index(String sortOrder, string currentFilter, string searchString, int? pageNumber, int? pSize)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentSize"] = pSize;
            ViewBag.PageSize = PaginatedList<VMProduct>.pageSizeList();


            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            //IQueryable<VMProduct> product = list.ToList().AsQueryable();

            var product = from p in _context.Products
                          join v in _context.Variants
                          on p.IdVariant equals v.IdVariant
                          join c in _context.Categories
                          on v.IdCategory equals c.IdCategory
                          // where v.IsDelete == false
                          select new VMProduct
                          {
                              IdProduct = p.IdProduct,
                              NameProduct = p.NameProduct,
                              Price = p.Price,
                              Stock = p.Stock,
                              NameVariant = v.NameVariant,
                              Image = p.Image
                          };

            if (!String.IsNullOrEmpty(searchString))
            {
                product = product.Where(s => s.NameProduct.Contains(searchString));

            }
            switch (sortOrder)
            {
                case "name_desc":
                    product = product.OrderByDescending(s => s.NameProduct);
                    break;
                default:
                    product = product.OrderBy(s => s.NameProduct);
                    break;
            }
            int pageSize = pSize ?? 5;
            countCart();
            return View(await PaginatedList<VMProduct>.CreateAsync(product, pageNumber ?? 1, pageSize));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public async Task<IActionResult> DetailOrder(int? Id)
        {
            VMProduct list = await (from p in _context.Products
                                    join v in _context.Variants
                                    on p.IdVariant equals v.IdVariant
                                    join c in _context.Categories
                                    on v.IdCategory equals c.IdCategory
                                    // where v.IsDelete == false
                                    select new VMProduct
                                    {
                                        IdProduct = p.IdProduct,
                                        NameProduct = p.NameProduct,
                                        Price = p.Price,
                                        Stock = p.Stock,
                                        Image = p.Image,
                                        NameVariant = v.NameVariant,
                                        NameCategory = c.NameCategory
                                    }).FirstOrDefaultAsync(a => a.IdProduct == Id);


            if (Id == 0)
            {
                return NotFound();
            }
            Product product = await _context.Products.FirstOrDefaultAsync(a => a.IdProduct == Id);

            if (product == null)
            {
                return NotFound();
            }
            return View(list);
        }

        public int countCart()
        {
            int count = _context.OrderDetails.Where(a=>a.IsDelete==false).Count();
            ViewBag.count = count;
            return count;
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
