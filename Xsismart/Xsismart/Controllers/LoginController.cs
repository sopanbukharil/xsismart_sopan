﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xsismart.Models;
using System.Net;
using System.Net.Mail;
using Xsismart.ViewModels;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Xsismart.Controllers
{
    public class LoginController : Controller
    {
        private readonly Xsismartcontext _context;

        public LoginController(Xsismartcontext context)
        {
            _context = context;
        }
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(Customer cust)
        {
           //// Customer custReady = CustExist(cust.Email, cust.Password);
            var user = _context.Customers.FirstOrDefault(u => u.Email == cust.Email);
            var isPasswordMatched = VerifyPassword(cust.Password, user.StoredSalt, user.Password);
            if ( isPasswordMatched)
            {
                HttpContext.Session.SetString("customername", user.NameCustomer);
                HttpContext.Session.SetInt32("idcustomer", user.IdCustomer);
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public IActionResult LogOut()
        {
            return RedirectToAction("Login");
        }

        private Customer CustExist(string email, string password)
        {
            return _context.Customers.FirstOrDefault(e => e.Email == email && e.Password == password);
        }

        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(VMCustomer customer)
        {
            if (ModelState.IsValid)
            {
                Customer cus = new Customer();
                cus.IdCustomer = customer.IdCustomer;
                cus.NameCustomer = customer.NameCustomer;
                cus.Email = customer.Email;
                cus.Password = customer.Password;
                cus.Address = customer.Address;

                var hashsalt = EncryptPassword(customer.Password);
                cus.Password = hashsalt.Hash;
                cus.StoredSalt = hashsalt.Salt;

                _context.Add(cus);
                await _context.SaveChangesAsync();
                return RedirectToAction("Login");
            }
            return View(customer);
        }

        public IActionResult otp(VMCustomer customer)
        {
          
            Random r = new Random();
            string OTP = r.Next(1000, 9999).ToString();
            customer.otp = OTP;

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);

            client.EnableSsl = true;

            MailAddress from = new MailAddress("reknotsru@gmail.com", "DONT REPLY!");

            MailAddress to = new MailAddress(customer.Email);

            MailMessage message = new MailMessage(from, to);

            message.Body = "Your OTP code is - " + OTP;

            message.Subject = "Verify";

            NetworkCredential myCreds = new NetworkCredential("reknotsru@gmail.com", "13101994", "");

            client.Credentials = myCreds;

            try

            {
                //HttpContext.Session.SetString("OTP", OTP);

                //Redirect for varification
                //Response.Redirect("VerifyOTP");
                client.Send(message);
                return Json(new { status = "success", data = customer });

            }

            catch (Exception ex)

            {
                return Json(new { status = "error"});
                //Console.WriteLine("Exception is:" + ex.ToString());

            }

            //Console.WriteLine("Goodbye.");
            return View();
        }

        public IActionResult VerifyOTP(VMCustomer customer)
        {
            return View(customer);
        }
        public string ValidateEmailId(string emailId)
        {
            Customer cust = new Customer();
            cust = _context.Customers.Where(a => a.Email == emailId).FirstOrDefault();
            if (cust != null)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }

        public HashSalt EncryptPassword(string password)
        {
            byte[] salt = new byte[128 / 8]; // Generate a 128-bit salt using a secure PRNG
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            string encryptedPassw = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));
            return new HashSalt { Hash = encryptedPassw, Salt = salt };
        }

        public bool VerifyPassword(string enteredPassword, byte[] salt, string storedPassword)
        {
            string encryptedPassw = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: enteredPassword,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            ));
            return encryptedPassw == storedPassword;
        }

    }
}

