﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xsismart.Models;
using Xsismart.ViewModels;

namespace Xsismart.Controllers
{
    public class TransactionController : Controller
    {
        private readonly Xsismartcontext _context;

        public TransactionController(Xsismartcontext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> AddCart(VMTransactions trx)
        {
            try
            {
                OrderHeader header = new OrderHeader();
                OrderDetail detail = new OrderDetail();
                DateTime dt = DateTime.Now;
                int d = dt.Day;
                int m = dt.Month;
                int y = dt.Year;
                string CodeTrx = $"XM-{d}{m}{y}-0001";
                string LastCode = await LastCodeTransaction();

                if (LastCode != "")
                {
                    string lastString = LastCode.Substring(LastCode.Length - 4, 4);
                    int lastInt = Convert.ToInt32(lastString) + 1;
                    string joinString = "0000" + lastInt.ToString();
                    CodeTrx = $"XM-{d}-{m}-{y}-" + joinString.Substring(joinString.Length - 4, 4);
                }
                header = await _context.OrderHeaders.Where(a => a.IdCustomer == 1 && a.IsCheckout == false).FirstOrDefaultAsync();

                if (header == null)
                {
                    header = new OrderHeader();
                    header.CodeTransaction = CodeTrx;
                    //  header.IdCustomer = trx.IdCustomer;
                    header.IsCheckout = false;
                    header.IdCustomer = 1;
                    _context.Add(header);
                    await _context.SaveChangesAsync();
                }

                detail.IdProduct = trx.IdProduct;
                detail.IdHeader = header.IdHeader;
                detail.Quantity = trx.Quantity;
                detail.Amount = trx.Amount;
                _context.Add(detail);
                await _context.SaveChangesAsync();

                return Json(new { status = "success" });
            }
            catch (Exception e)
            {
                return Json(new { status = "error", message = e.Message.ToString() });
            }
        }

        public async Task<string> LastCodeTransaction()
        {
            string lastNoTrx = "";
            OrderHeader data = await _context.OrderHeaders.
                                     OrderByDescending(a => a.CodeTransaction).FirstOrDefaultAsync();
            if (data != null)
            {
                lastNoTrx = data.CodeTransaction;
            }
            return lastNoTrx;
        }
        public async Task<IActionResult> Invoice()
        {
            int idCust = 1;
            List<VMTransactions> list = await DataTransaction(idCust);
            DateTime date = DateTime.Now;

            decimal subtotal = await dataSumOrder(idCust);
            string subtotal1 = string.Format("{0:0,0.00}", subtotal);
            double tax = ((double)5 / 100) * ((double)subtotal);
            ViewBag.DateNow = date;
            ViewBag.SubTotal = subtotal1;
            ViewBag.IdHeader = list.Select(a => a.IdHeader).FirstOrDefault();
            ViewBag.IdCustomer = list.Select(a => a.IdCustomer).FirstOrDefault();
            ViewBag.IsCheckout = list.Select(a => a.IsCheckout).FirstOrDefault();


            ViewBag.Tax = tax;
            ViewBag.Total = ((double)subtotal + tax);
            ViewBag.CodeTransaction = list.Select(a => a.CodeTransaction).FirstOrDefault();

            return View(list);
        }

        public async Task<decimal> dataSumOrder(int idCust)
        {
            decimal subtotal = 0;
            subtotal = await (from head in _context.OrderHeaders
                              join detail in _context.OrderDetails
                              on head.IdHeader equals detail.IdHeader
                              where head.IdCustomer == idCust && head.IsCheckout == false
                              select detail.Amount).SumAsync();
            return subtotal;
        }

        public async Task<List<VMTransactions>> DataTransaction(int IdCustomer)
        {
            List<VMTransactions> list = await (from head in _context.OrderHeaders
                                               join detail in _context.OrderDetails
                                               on head.IdHeader equals detail.IdHeader
                                               join prod in _context.Products
                                               on detail.IdProduct equals prod.IdProduct
                                               join vari in _context.Variants
                                               on prod.IdVariant equals vari.IdVariant
                                               join cate in _context.Categories
                                               on vari.IdCategory equals cate.IdCategory
                                               where head.IdCustomer == IdCustomer && head.IsCheckout == false
                                               select new VMTransactions
                                               {
                                                   IdProduct = prod.IdProduct,
                                                   NameProduct = prod.NameProduct,
                                                   Price = prod.Price,
                                                   IdCustomer = head.IdCustomer,
                                                   IsCheckout = head.IsCheckout,
                                                   IdDetail = detail.IdDetail,
                                                   NameCategory = cate.NameCategory,
                                                   IdCategory = cate.IdCategory,
                                                   NameVariant = vari.NameVariant,
                                                   IdHeader = head.IdHeader,
                                                   CodeTransaction = head.CodeTransaction,
                                                   Amount = detail.Amount,
                                                   Quantity = detail.Quantity
                                               }).ToListAsync();

            return list;
        }

        public async Task<IActionResult> Delete(int id)
        {
            if (id == null)
            {
                return NotFound();
            }

            VMTransactions orderDetail = await DataTrans(id);
            if (orderDetail == null)
            {
                return NotFound();
            }

            return View(orderDetail);
        }

        // POST: OrderDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(OrderDetail _detail)
        {
            if (ModelState.IsValid)
            {
                _context.Remove(_detail);
                await _context.SaveChangesAsync();
                return RedirectToAction("Invoice");
            }
            return View(_detail);
        }

     
        public async Task<VMTransactions> DataTrans(int id)
        {
            VMTransactions list = await (from head in _context.OrderHeaders
                                               join detail in _context.OrderDetails
                                               on head.IdHeader equals detail.IdHeader
                                               join prod in _context.Products
                                               on detail.IdProduct equals prod.IdProduct
                                               join vari in _context.Variants
                                               on prod.IdVariant equals vari.IdVariant
                                               join cate in _context.Categories
                                               on vari.IdCategory equals cate.IdCategory
                                               where detail.IdDetail == id && head.IsCheckout == false
                                               select new VMTransactions
                                               {
                                                   IdProduct = prod.IdProduct,
                                                   NameProduct = prod.NameProduct,
                                                   Price = prod.Price,

                                                   IdDetail = detail.IdDetail,
                                                   NameCategory = cate.NameCategory,
                                                   IdCategory = cate.IdCategory,
                                                   NameVariant = vari.NameVariant,
                                                   IdHeader = head.IdHeader,
                                                   CodeTransaction = head.CodeTransaction,
                                                   Amount = detail.Amount,
                                                   Quantity = detail.Quantity
                                               }).FirstOrDefaultAsync();
            return list;

        }

        [HttpPost]
        public async Task<IActionResult> Checkout(VMTransactions trx)
        {
         
            List<OrderDetail> detail = await DataOrder(trx.IdHeader);

            OrderHeader header = await _context.OrderHeaders.Where(a => a.IdCustomer == trx.IdCustomer && a.IdHeader == trx.IdHeader && a.IsCheckout == trx.IsCheckout).FirstOrDefaultAsync();
           
            try
            {
                header.IsCheckout = true;
                int counter = detail.Count();
                for (int i = 0; i < counter; i++)
                {
                    detail[i].IsDelete = true;
                    _context.Update(detail[i]);
                }
                _context.Update(header);
                await _context.SaveChangesAsync();
                //return RedirectToAction("Index");
                return Json(new { status = "success" });
            }
            catch (Exception e)
            {
                return Json(new { status = "error", message = e.Message.ToString() });
            }
        }

        public async Task<List<OrderDetail>> DataOrder(int IdHeader)
        {
            List<OrderDetail> va = await _context.OrderDetails.
                                        Where(a => a.IsDelete == false && a.IdHeader.Equals(IdHeader)).ToListAsync();
            return va;
        }
    }
}
