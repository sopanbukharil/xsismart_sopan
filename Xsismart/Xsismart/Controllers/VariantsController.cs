﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Xsismart.Models;
using Xsismart.ViewModels;

namespace Xsismart.Controllers
{
    public class VariantsController : Controller
    {
        private readonly Xsismartcontext _context;

        public VariantsController(Xsismartcontext context)
        {
            _context = context;
        }

        // GET: Variants
        public async Task<IActionResult> Index()
        {
            List<VMVariants> list = await (from v in _context.Variants
                                           join c in _context.Categories
                                           on v.IdCategory equals c.IdCategory
                                           where v.IsDelete == false
                                           select new VMVariants
                                           {
                                               IdVariant = v.IdVariant,
                                               NameVariant = v.NameVariant,
                                               Description = v.Description,
                                               NameCategory = c.NameCategory
                                           }).ToListAsync();
                                               
            return View(list);
        }

        // GET: Variants/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            VMVariants list = await (from v in _context.Variants
                                           join c in _context.Categories
                                           on v.IdCategory equals c.IdCategory
                                           where v.IsDelete == false
                                           select new VMVariants
                                           {
                                               IdVariant = v.IdVariant,
                                               NameVariant = v.NameVariant,
                                               Description = v.Description,
                                               NameCategory = c.NameCategory
                                           }).FirstOrDefaultAsync(m => m.IdVariant == id);

            if (id == null)
            {
                return NotFound();
            }

            var variant = await _context.Variants
                .FirstOrDefaultAsync(m => m.IdVariant == id);
            if (variant == null)
            {
                return NotFound();
            }

            return View(list);
        }

        // GET: Variants/Create
        public async Task <IActionResult> Create()
        {
            await DropDown();
            return View();
        }

        // POST: Variants/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdVariant,NameVariant,Description,IsDelete,IdCategory")] Variant variant)
        {
            if (ModelState.IsValid)
            {
                _context.Add(variant);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(variant);
        }

        // GET: Variants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            List<Category> variants = new List<Category>();
            variants = (from v in _context.Categories select v).ToList();
            variants.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category--", Description = "" });
            ViewBag.message = variants;
            

            if (id == null)
            {
                return NotFound();
            }

            var variant = await _context.Variants.FindAsync(id);
            if (variant == null)
            {
                return NotFound();
            }


            return View(variant);
        }

        // POST: Variants/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdVariant,NameVariant,Description,IsDelete,IdCategory")] Variant variant)
        {
            if (id != variant.IdVariant)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(variant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VariantExists(variant.IdVariant))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(variant);
        }

        // GET: Variants/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var variant = await _context.Variants
                .FirstOrDefaultAsync(m => m.IdVariant == id);
            if (variant == null)
            {
                return NotFound();
            }

            return View(variant);
        }

        // POST: Variants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id, [Bind("IdVariant,NameVariant,Description,IsDelete,IdCategory")] Variant variant)
        {
            if (id != variant.IdVariant)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(variant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VariantExists(variant.IdVariant))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(variant);
        }

        private bool VariantExists(int id)
        {
            return _context.Variants.Any(e => e.IdVariant == id);
        }

        public async Task<List<Category>> DropDown()
        {
            List<Category> cat = await (from c in _context.Categories select c).ToListAsync();
            cat.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category Name--" });
            ViewBag.message = cat;
            return cat;
        }
    }
}
