﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xsismart.Misc;
using Xsismart.Models;
using Xsismart.ViewModels;

namespace Xsismart.Controllers
{
    public class ProductController : Controller
    {
        private readonly Xsismartcontext _context;

        private readonly IWebHostEnvironment _webHostEnvironment;
        public ProductController(Xsismartcontext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }
        public async Task<IActionResult> Index(String sortOrder, string currentFilter, string searchString, int? pageNumber, int? pSize)
        {
            

            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["CurrentSize"] = pSize;
            ViewBag.PageSize = PaginatedList<VMProduct>.pageSizeList();


            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            //IQueryable<VMProduct> product = list.ToList().AsQueryable();

            var product = from p in _context.Products
                          join v in _context.Variants
                          on p.IdVariant equals v.IdVariant
                          join c in _context.Categories
                          on v.IdCategory equals c.IdCategory
                          // where v.IsDelete == false
                          select new VMProduct
                          {
                              IdProduct = p.IdProduct,
                              NameProduct = p.NameProduct,
                              Price = p.Price,
                              Stock = p.Stock,
                              NameVariant = v.NameVariant
                          };

            if (!String.IsNullOrEmpty(searchString))
            {
                product = product.Where(s => s.NameProduct.Contains(searchString));
                                       
            }
            switch (sortOrder)
            {
                case "name_desc":
                    product = product.OrderByDescending(s => s.NameProduct);
                    break;
                default:
                    product = product.OrderBy(s => s.NameProduct);
                    break;
            }
            int pageSize = pSize ?? 5;
            return View(await PaginatedList<VMProduct>.CreateAsync(product, pageNumber ?? 1, pageSize));


            //List<Product> listProduct = new List<Product>()
            //{
            //    new Product(){IdProduct = 1, NameProduct="Kopi Kapal Api", Price = 5000, Stock = 10 },
            //    new Product(){IdProduct = 2, NameProduct="Sari Roti Coklat", Price = 15000, Stock = 10 },
            //    new Product(){IdProduct = 3, NameProduct="Chitato", Price = 9000, Stock = 10 }
            //};

            //    ViewBag.ListProduct = listProduct;

        }

        public async Task<IActionResult> Create()
        {
            List<Category> category = new List<Category>();
            category = (from c in _context.Categories select c).ToList();
            category.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category--" });
            ViewBag.abc = category;

            List<Variant> variants = new List<Variant>();
            variants.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant--", Description = "", IdCategory = 0 });
            ViewBag.message = variants;



            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Product _product)
        {
            //Product prodNew = new Product();

            Product prodNew = _product;
            if (ModelState.IsValid)
            {
                _context.Add(prodNew);
                // _context.SaveChanges();
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            List<Category> listCat = await ListCategory();
            List<Variant> variants = new List<Variant>();
            variants.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant--", Description = "", IdCategory = 0 });
            ViewBag.abc = listCat;
            ViewBag.message = variants;

            return View(_product);
        }

        public async Task<IActionResult> Detail(int Id)
        {
            VMProduct list = await (from p in _context.Products
                                    join v in _context.Variants
                                    on p.IdVariant equals v.IdVariant
                                    join c in _context.Categories
                                    on v.IdCategory equals c.IdCategory
                                    // where v.IsDelete == false
                                    select new VMProduct
                                    {
                                        IdProduct = p.IdProduct,
                                        NameProduct = p.NameProduct,
                                        Price = p.Price,
                                        Stock = p.Stock,
                                        NameVariant = v.NameVariant,
                                        NameCategory = c.NameCategory
                                    }).FirstOrDefaultAsync(a => a.IdProduct == Id);


            if (Id == 0)
            {
                return NotFound();
            }
            Product product = await _context.Products.FirstOrDefaultAsync(a => a.IdProduct == Id);

            if (product == null)
            {
                return NotFound();
            }
            return View(list);
        }

        public async Task<IActionResult> Edit(int Id)
        {
            ViewBag.listCat = await ListCategory();
            // List<Variant> variants = new List<Variant>();
            //variants = (from c in _context.Variants select c).ToList();
            // variants.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant--", Description = "", IdCategory = 0 });


            if (Id == 0)
            {
                return NotFound();
            }
            // var product = await _context.Products.FindAsync(Id);
            VMProduct product = await dataVP(Id);

            if (product == null)
            {
                return NotFound();
            }

            ViewBag.listVar = await DataVariant(product.IdCategory);
            return View(product);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMProduct _product)
        {
            //Product prodNew = new Product();


            if (ModelState.IsValid)
            {
                Product prod = new Product();
                prod.NameProduct = _product.NameProduct;
                prod.Price = _product.Price;
                prod.Stock = _product.Stock;
                prod.IdVariant = _product.IdVariant;

                _context.Update(_product);
                // _context.SaveChanges();
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            List<Category> listCat = await ListCategory();
            ViewBag.listCat = listCat;
            ViewBag.listVar = await DataVariant(_product.IdCategory);

            return View(_product);
        }

        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .FirstOrDefaultAsync(m => m.IdProduct == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Products.FindAsync(id);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExist(int id)
        {
            return _context.Products.Any(e => e.IdProduct == id);
        }

        public async Task<List<Variant>> DataVariant(int IdCategory)
        {
            List<Variant> list = await _context.Variants.
                            Where(a => a.IsDelete == false && a.IdCategory.Equals(IdCategory)).ToListAsync();
            list.Insert(0, new Variant { IdVariant = 0, NameVariant = "--Select Variant Name--" });
            return list;
        }

        public async Task<List<Category>> ListCategory()
        {
            List<Category> category = await (from c in _context.Categories select c).ToListAsync();
            category.Insert(0, new Category { IdCategory = 0, NameCategory = "--Select Category--" });
            return category;
        }

        public async Task<JsonResult> GetDataVariantAjax(int IdCategory)
        {
            List<Variant> list = await DataVariant(IdCategory);
            return Json(list);
        }

        public async Task<VMProduct> dataVP(int Id)
        {
            VMProduct list = await (from p in _context.Products
                                    join v in _context.Variants
                                    on p.IdVariant equals v.IdVariant
                                    join c in _context.Categories
                                    on v.IdCategory equals c.IdCategory
                                    // where v.IsDelete == false
                                    select new VMProduct
                                    {
                                        IdProduct = p.IdProduct,
                                        NameProduct = p.NameProduct,
                                        Price = p.Price,
                                        Stock = p.Stock,
                                        IdVariant = v.IdVariant,
                                        IdCategory = c.IdCategory,
                                        NameVariant = v.NameVariant,
                                        NameCategory = c.NameCategory
                                    }).FirstOrDefaultAsync(a => a.IdProduct == Id);
            return list;
        }

        [HttpPost]
        public IActionResult OnPostMyUploader(IFormFile MyUploader)
        {
            if (MyUploader != null)
            {
                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(MyUploader.FileName);
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "mediaUpload");
                string filePath = Path.Combine(uploadsFolder, fileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    MyUploader.CopyTo(fileStream);
                }
                return Json(new { status = "success",fileName = fileName });
            }
            return new ObjectResult(new { status = "fail" });

        }
    }
}
