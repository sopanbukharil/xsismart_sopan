﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xsismart.Models
{
    public class OrderHeader
    {
        public int IdHeader { get; set; }
        public string CodeTransaction { get; set; }
        public int IdCustomer { get; set; }
        public bool IsCheckout { get; set; }
        public bool IsDelete { get; set; }
    }
}
