﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Xsismart.Models
{
    public class Variant
    {
        [Required]
        public int IdVariant { get; set; }
        [Required]
        public string NameVariant { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public bool? IsDelete { get; set; }
        [Required]
        public int IdCategory { get; set; }


    }
}
