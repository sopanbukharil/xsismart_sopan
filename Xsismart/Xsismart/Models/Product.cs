﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Xsismart.Models
{
    public class Product
    {
       
        public int IdProduct { get; set; }
        [Required(ErrorMessage = "Name length can't be more than 8.")]
        public string NameProduct { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public int Stock { get; set; }
        public int IdVariant { get; set; }

        public int IdCategory { get; set; }

        public string Image { get; set; }
    }
}
