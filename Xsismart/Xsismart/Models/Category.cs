﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Xsismart.Models
{
    public class Category
    {
       [Required]
        public int IdCategory { get; set; }
        [Required]
        public string NameCategory { get; set; }
        [Required]
        public string Description { get; set; }

        public bool IsDelete { get; set; }
    }
}
