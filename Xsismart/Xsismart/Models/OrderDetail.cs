﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xsismart.Models
{
    public class OrderDetail
    {
        public int IdDetail { get; set; }
        public int IdHeader { get; set; }
        public int IdProduct { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public bool IsDelete { get; set; }

    }
}
