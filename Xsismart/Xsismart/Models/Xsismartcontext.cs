﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Xsismart.Models
{
    public class Xsismartcontext : DbContext
    {
        public Xsismartcontext(DbContextOptions<Xsismartcontext> options) : base(options)
        {
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Variant> Variants { get; set; }

        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<OrderHeader> OrderHeaders { get; set; }

        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().HasKey(e => new { e.IdProduct });
            modelBuilder.Entity<Category>().HasKey(e => new { e.IdCategory });
            modelBuilder.Entity<Variant>().HasKey(e => new { e.IdVariant });
            modelBuilder.Entity<OrderHeader>().HasKey(e => new { e.IdHeader });
            modelBuilder.Entity<OrderDetail>().HasKey(e => new { e.IdDetail });
            modelBuilder.Entity<Customer>().HasKey(e => new { e.IdCustomer });

        }
    }
}
