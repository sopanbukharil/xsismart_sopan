#pragma checksum "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\Home\DetailOrder.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1c5408157cf5557650aee55c525f6a0983092b2b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_DetailOrder), @"mvc.1.0.view", @"/Views/Home/DetailOrder.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\_ViewImports.cshtml"
using Xsismart;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\_ViewImports.cshtml"
using Xsismart.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1c5408157cf5557650aee55c525f6a0983092b2b", @"/Views/Home/DetailOrder.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"287ecf493fbb6ba68e4c4b18feacbd7dc022116c", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_DetailOrder : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Xsismart.ViewModels.VMProduct>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("product-image"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("Product Image"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "AddCart", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Transaction", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "POST", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("form-transaction"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
#nullable restore
#line 3 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\Home\DetailOrder.cshtml"
   ViewData["Title"] = "Details";
    Layout = null; 

#line default
#line hidden
#nullable disable
            WriteLiteral("\n<section class=\"content\">\n\n    <!-- Default box -->\n    <div class=\"card card-solid\">\n        <div class=\"card-body\">\n            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "1c5408157cf5557650aee55c525f6a0983092b2b5708", async() => {
                WriteLiteral(@"
                <div class=""row"">
                    <div class=""col-12 col-sm-6"">
                        <h3 class=""d-inline-block d-sm-none"">LOWA Men’s Renegade GTX Mid Hiking Boots Review</h3>
                        <div class=""col-12"">
                            ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c5408157cf5557650aee55c525f6a0983092b2b6243", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                AddHtmlAttributeValue("", 598, "~/mediaUpload/", 598, 14, true);
#nullable restore
#line 16 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\Home\DetailOrder.cshtml"
AddHtmlAttributeValue("", 612, Model.Image, 612, 12, false);

#line default
#line hidden
#nullable disable
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\n                        </div>\n");
                WriteLiteral("                    </div>\n                    <div class=\"col-12 col-sm-6\">\n                        <h3 class=\"my-3\">");
#nullable restore
#line 27 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\Home\DetailOrder.cshtml"
                                    Write(Model.NameProduct);

#line default
#line hidden
#nullable disable
                WriteLiteral(@"</h3>
                        <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terr.</p>

                        <hr>
                        <label>Quantity :</label>
                        <input type=""number"" min=""0"" class=""form-control col-2 "" id=""Quantity"" name=""Quantity"" />
                        <span style=""color:red;display:none"" id=""validateQty""></span>

                        <input type=""hidden"" class=""form-control"" id=""Stock"" name=""Stock""");
                BeginWriteAttribute("value", " value=\"", 2181, "\"", 2201, 1);
#nullable restore
#line 35 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\Home\DetailOrder.cshtml"
WriteAttributeValue("", 2189, Model.Stock, 2189, 12, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\n                        <input type=\"hidden\" class=\"form-control\" id=\"IdProduct\" name=\"IdProduct\"");
                BeginWriteAttribute("value", " value=\"", 2303, "\"", 2327, 1);
#nullable restore
#line 36 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\Home\DetailOrder.cshtml"
WriteAttributeValue("", 2311, Model.IdProduct, 2311, 16, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\n                        <input type=\"hidden\" class=\"form-control\" id=\"Price\" name=\"Price\"");
                BeginWriteAttribute("value", " value=\"", 2421, "\"", 2441, 1);
#nullable restore
#line 37 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\Home\DetailOrder.cshtml"
WriteAttributeValue("", 2429, Model.Price, 2429, 12, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\n\n\n                        <div class=\"bg-gray py-2 px-3 mt-4\">\n                            <h2 class=\"mb-0\">\n                                Rp. ");
#nullable restore
#line 42 "D:\LATIHAN BOOTCAMP\Xsismart\Xsismart\Views\Home\DetailOrder.cshtml"
                               Write(Model.Price);

#line default
#line hidden
#nullable disable
                WriteLiteral(@"
                            </h2>

                        </div>

                        <div class=""mt-4"">

                            <div id=""btn-cart"" class=""btn btn-default btn-lg btn-flat"">
                                <i class=""fas fa-cart-plus fa-lg mr-2""></i>
                                Add to Cart
                            </div>
                            <div class=""btn btn-default btn-lg btn-flat"">
                                <i class=""fas fa-heart fa-lg mr-2""></i>
                                Add to Wishlist
                            </div>
                        </div>
                    </div>


                </div>
            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
        </div>
        <div class=""row mt-4"">
            <nav class=""w-100"">
                <div class=""nav nav-tabs"" id=""product-tab"" role=""tablist"">
                    <a class=""nav-item nav-link active"" id=""product-desc-tab"" data-toggle=""tab"" href=""#product-desc"" role=""tab"" aria-controls=""product-desc"" aria-selected=""true"">Description</a>
                    <a class=""nav-item nav-link"" id=""product-comments-tab"" data-toggle=""tab"" href=""#product-comments"" role=""tab"" aria-controls=""product-comments"" aria-selected=""false"">Comments</a>
                    <a class=""nav-item nav-link"" id=""product-rating-tab"" data-toggle=""tab"" href=""#product-rating"" role=""tab"" aria-controls=""product-rating"" aria-selected=""false"">Rating</a>
                </div>
            </nav>
            <div class=""tab-content p-3"" id=""nav-tabContent"">
                <div class=""tab-pane fade show active"" id=""product-desc"" role=""tabpanel"" aria-labelledby=""product-desc-tab""> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morb");
            WriteLiteral(@"i vitae condimentum erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed posuere, purus at efficitur hendrerit, augue elit lacinia arcu, a eleifend sem elit et nunc. Sed rutrum vestibulum est, sit amet cursus dolor fermentum vel. Suspendisse mi nibh, congue et ante et, commodo mattis lacus. Duis varius finibus purus sed venenatis. Vivamus varius metus quam, id dapibus velit mattis eu. Praesent et semper risus. Vestibulum erat erat, condimentum at elit at, bibendum placerat orci. Nullam gravida velit mauris, in pellentesque urna pellentesque viverra. Nullam non pellentesque justo, et ultricies neque. Praesent vel metus rutrum, tempus erat a, rutrum ante. Quisque interdum efficitur nunc vitae consectetur. Suspendisse venenatis, tortor non convallis interdum, urna mi molestie eros, vel tempor justo lacus ac justo. Fusce id enim a erat fringilla sollicitudin ultrices vel metus. </div>
                <div class=""tab-pane fade"" id=""product-comments"" role=""tabpanel"" aria");
            WriteLiteral(@"-labelledby=""product-comments-tab""> Vivamus rhoncus nisl sed venenatis luctus. Sed condimentum risus ut tortor feugiat laoreet. Suspendisse potenti. Donec et finibus sem, ut commodo lectus. Cras eget neque dignissim, placerat orci interdum, venenatis odio. Nulla turpis elit, consequat eu eros ac, consectetur fringilla urna. Duis gravida ex pulvinar mauris ornare, eget porttitor enim vulputate. Mauris hendrerit, massa nec aliquam cursus, ex elit euismod lorem, vehicula rhoncus nisl dui sit amet eros. Nulla turpis lorem, dignissim a sapien eget, ultrices venenatis dolor. Curabitur vel turpis at magna elementum hendrerit vel id dui. Curabitur a ex ullamcorper, ornare velit vel, tincidunt ipsum. </div>
                <div class=""tab-pane fade"" id=""product-rating"" role=""tabpanel"" aria-labelledby=""product-rating-tab""> Cras ut ipsum ornare, aliquam ipsum non, posuere elit. In hac habitasse platea dictumst. Aenean elementum leo augue, id fermentum risus efficitur vel. Nulla iaculis malesuada scelerisque. Praesent ve");
            WriteLiteral(@"l ipsum felis. Ut molestie, purus aliquam placerat sollicitudin, mi ligula euismod neque, non bibendum nibh neque et erat. Etiam dignissim aliquam ligula, aliquet feugiat nibh rhoncus ut. Aliquam efficitur lacinia lacinia. Morbi ac molestie lectus, vitae hendrerit nisl. Nullam metus odio, malesuada in vehicula at, consectetur nec justo. Quisque suscipit odio velit, at accumsan urna vestibulum a. Proin dictum, urna ut varius consectetur, sapien justo porta lectus, at mollis nisi orci et nulla. Donec pellentesque tortor vel nisl commodo ullamcorper. Donec varius massa at semper posuere. Integer finibus orci vitae vehicula placerat. </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    <!-- /.card -->

</section>
<!-- /.content -->
<script type=""text/javascript"">
    $(""#btn-cart"").click(function () {
        
        var qty = $(""#Quantity"").val();
        var stock = $(""#Stock"").val();
        var price = $(""#Price"").val();

        if (qty == """") {
            $(""#validateQty"").html");
            WriteLiteral(@"('Please enter quantity')
            $(""#validateQty"").show();
            return false;
        }
        if (parseInt(qty) > parseInt(stock)) {
            $(""#validateQty"").html('Purchase over quantity')
            $(""#validateQty"").show();
            return false;
        }
        $(""#validateQty"").hide();
        var dataForm = $(""#form-transaction"").serialize();
        var amount = parseInt(qty) * parseInt(price)
        dataForm = dataForm + '&Amount=' + amount

       
        $.ajax({
            url: '/Transaction/AddCart',
            data: dataForm,
            method: 'post',
            success: function (respon) {
                if (respon.status == ""success"") {
                    toastr.success('Thanks for your order')
                    $('#model-default').modal('hide');

                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
            }
        })
    })
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Xsismart.ViewModels.VMProduct> Html { get; private set; }
    }
}
#pragma warning restore 1591
