﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Xsismart.Migrations
{
    public partial class xsismart_v004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsDelete",
                table: "Variants",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IdCategory",
                table: "Variants",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdVariant",
                table: "Products",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdCategory",
                table: "Variants");

            migrationBuilder.DropColumn(
                name: "IdVariant",
                table: "Products");

            migrationBuilder.AlterColumn<int>(
                name: "IsDelete",
                table: "Variants",
                type: "int",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
