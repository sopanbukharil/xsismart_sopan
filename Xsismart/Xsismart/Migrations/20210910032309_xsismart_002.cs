﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Xsismart.Migrations
{
    public partial class xsismart_002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Variants",
                columns: table => new
                {
                    IdVariant = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameVariant = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsDelete = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variants", x => x.IdVariant);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Variants");
        }
    }
}
