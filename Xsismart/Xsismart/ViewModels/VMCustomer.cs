﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xsismart.ViewModels
{
    public class VMCustomer
    {
        public int IdCustomer { get; set; }
        public string NameCustomer { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string otp { get; set; }
        
    }
}
