﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xsismart.ViewModels
{
    public class VMTransactions
    {
        public int IdDetail { get; set; }
        public int IdHeader { get; set; }
        public int IdProduct { get; set; }
        public string NameProduct { get; set; }

        public int IdCategory { get; set; }
        public string NameCategory { get; set; }

        public int IdVariant { get; set; }
        public string NameVariant { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public decimal Price { get; set; }

        public string CodeTransaction { get; set; }
        public int IdCustomer { get; set; }
        public bool IsCheckout { get; set; }
    
    }
}
